<?php

/**
 * @file
 * The hooks that are used by Coffee
 * includes an example of hook_coffee_command()
 */

/**
 * The default implementation of Coffee.
 * It will look in the menu_links table for administrative links based on
 * the user input.
 *
 * @param string $input
 *   The user input that will be used to query the database.
 */
function coffee_redirect($input) {

  // Glue the arguments together if there are more than 3 arguments.
  // This happens when the user $input typed a / in the input field.
  $arguments = arg();
  if (count($arguments) > 3) {
    $argument_query = array_splice($arguments, 3);
    $input = implode('/', $argument_query);
  }

  $input = check_plain($input);

  // Build the query for the result set.
  // Look up items in the menu_links table.
  // Limit the result set to 7.
  $sql = "
SELECT 
  ml.link_path AS link_path, ml.link_title AS link_title
FROM
  {menu_links} ml
INNER JOIN
  {menu_router} mr
ON
  ml.link_path = mr.path
WHERE
  (ml.menu_name IN ('navigation', 'devel')) AND
  ((ml.link_path LIKE '%%%s%%') OR (ml.link_title LIKE '%%%s%%') OR (mr.description LIKE '%%%s%%')) AND
  (ml.link_path NOT LIKE '%\%%%') AND
  (ml.link_path NOT LIKE '%help%') AND
  (ml.link_title <> '%%%%')
ORDER BY
  ml.link_title ASC
";

  // Execute the query above.
  $result = db_query($sql, array($input, $input, $input));

  $return = array();
  while ($record = db_fetch_object($result)) {
    // Load the menu item and check if the user has persmission.
    $item = menu_get_item($record->link_path);

    // Only add items to the return array if the user has access.
    if ($item['access']) {
      $match = array(
        'path' => $record->link_path,
        'title' => $record->link_title,
      );
      // Make this the top item if the title is an exact match.
      if (drupal_strtolower(trim($record->link_title)) == drupal_strtolower($input)) {
        array_unshift($return, $match);
      }
      else {
        $return[] = $match;
      }
    }
  }

  if (!count($return)) {
    return FALSE;
  }

  // Return only a slice of a maximum of 7 results.
  if (is_array($return)) {
    $number_of_items = variable_get('coffee_number_of_results', 7);
    $return = array_slice($return, 0, $number_of_items);
  }

  return $return;
}

/**
 * Implementation of hook_coffee_command().
 */
function coffee_coffee_command($op) {
  switch ($op) {

    // Example usage of how a result array should be formatted.
    case 'help' :
      $return['help'] = array(
        'path' => 'admin/help/coffee',
        'title' => 'Coffee Help',
      );
      break;

    // Display the links of the node/add page.
    case 'add' :
      // This method is used in the core nodes module.
      $path = 'node/add';
      $item = menu_get_item($path);
      $content_types = system_admin_menu_block($item);

      $return = array();
      foreach ($content_types as $content_type) {
        $return[] = array(
          'path' => $content_type['link_path'],
          'title' => $content_type['link_title'],
        );
      }
      break;

  }

  if (isset($return)) {
    return $return;
  }
}
